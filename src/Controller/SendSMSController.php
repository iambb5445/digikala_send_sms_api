<?php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Config\Definition\Exception\Exception;

class SendSMSController
{
    /**
     * @Route("/send/sms")
     */
    public function sendSMS(Request $request)
    {
        //getting parameters and checking
        $number = $request->query->get("number");
        $body = $request->query->get("body");
        if (!isset($number))
            throw new Exception('number parameter is empty.');
        if (!isset($body))
            throw new Exception('body parameter is empty');
        // length and validation of number can be checked
        // length and characters of body can be checked


        return new Response('<html><body>sms "'.$body.'" sent to '.$number.'</body></html>');
    }
}