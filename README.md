#Send SMS API
simulating an SMS sending system that uses two mock APIs as follows:

localhost:81/send/sms?number={phone_number}&body={sms_body}
localhost:82/send/sms?number={phone_number}&body={sms_body}

the API itself will run on port 80 as:

localhost:80/send/sms?number={phone_number}&body={sms_body}

## Getting Started
use following commands to run this project on your local machine.

###Prerequisites
you need to download symfony for this project.

dependencies can be installed using composer with following command (use in project directory):
```
composer install
```

### Running
after installing dependencies, use the following command to run the project on port 80:
```
symfony server:start --port=80
```